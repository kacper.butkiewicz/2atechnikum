class Wydatek:
    def __init__(self,nazwa,kwota,typ="towar"):
        self.nazwa = nazwa
        self.kwota = kwota
        self.typ = typ

    def __str__(self):
        return(self.nazwa + " " + str(self.kwota) + " " + self.typ)



# klasa Lista z atrybutem lista_produktow przechowująca każdy dodany wydatek
class Lista_wydatkow:
    def __init__(self,lista_produktow):
        self.lista_produktow = lista_produktow

    # funkcja wypisująca po kolei wszystkie dodane wydatki
    def wyswietl_wszystkie(self):
        number = 1
        for o in self.lista_produktow:
            print(str(number) + ". " + o.__str__())
            number += 1

    def wyswietl_uslugi(self):
        number = 1
        for o in self.lista_produktow:
            if o.typ == "usluga":
                print(str(number) + ". " + o.__str__())
                number += 1



ob1 = Wydatek("lays",4.50)
ob2 = Wydatek("coca cola",6)
ob3 = Wydatek("fryzier",25,"usluga")

ww= Lista_wydatkow ([ob1,ob2,ob3])
ww.wyswietl_wszystkie()
ww.wyswietl_uslugi()

